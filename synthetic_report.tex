\documentclass{aoareport}

\begin{document}

\aoatitle{Pre-study on the Development of a Swedish synthetic population}
         {Oskar Allerbo$^1$, Devdatt Dubashi$^1$, Holger Wallbaum$^2$, Anders Logg$^3$, Magne Nordaas$^3$}
         {$^1$Computer Science, $^2$Building Technology, $^3$Mathematical Sciences}
         {BUILDING FUTURES \\[1em] INFORMATION AND COMMUNICATION TECHNOLOGY}

\section{Synthetic populations: What and why}
\label{sec:intro}

A synthetic population is a representation of a real population,
constructed in such a way that the population as a whole is
statistically indistinguishable from the real population, but at the
same time the synthetic individuals do not correspond to real
individuals. In this way one can in some sense both have the cake and
eat it: The population is described down to individual level, enabling
very sophisticated agent based modeling, at the same time as personal
integrity is not compromised.

The population consists of synthetic individuals that have demographic
attributes such as age, gender and income, but also geographic ones as
coordinates of home and work place. On top of that each person has an
activity pattern, or schedule, where it performs different activities
at different locations, so that a normative day can be simulated.

The first synthetic populations that preserved correlations between
people's attributes (e.g. education and income level) were created at
the Los Alamos National Laboratory in 1996 when so called Iterative
Proportional Fitting was introduced~\cite{beckman}. This technique
uses a subsample of the population and combines it with census data to
construct the synthetic population.

Synthetic populations were originally developed as a response to the
increasing demand for individual agents in activity-based
transportation models, particularly TRANSIMS~\cite{transims} that was
simultaneously developed at the Los Alamos Laboratory. Over the years
the applications of synthetic populations have grown to include
modelling the spreading of infectious diseases~\cite{episimdemics} and
urban planning~\cite{urbansim}, and since it can be extended to any
problem that can be modeled by people moving around and interacting
with each other, we will most certainly see multiple new fields of
application in the future.

\subsection{Goals and results achieved}

The goals of this pre-study were:
\begin{itemize}
\item \textbf{To identify the interest at Chalmers for a synthetic
    population of Sweden.}\\
  We have already expressions of interest and initial discussions with
  several groups at Chalmers in the AoAs Energy, Building Futures and
  Life Science and Engineering, see Section~\ref{sec:apps}.
\item \textbf{To explore the current state-of-the-art in synthetic population
modelling.}\\
This is summarized in Section~\ref{sec:intro}.
\item \textbf{To identify optimization potential in the current models.}\\
  Some optimization has already been done, see
  Section~\ref{sec:synthswe}, while some potential optimization is
  identified, but not yet performed, as described in
  Section~\ref{sec:future}.
\item \textbf{To create a solid methodological approach for follow-up
    projects.}\\
  We have outlined these below, see especially
  Sections~\ref{sec:collab} and~\ref{sec:future}.
\item \textbf{To consolidate the research cooperation with the Network
    Dynamics and Simulation Science Laboratory, Virginia
    Bioinformatics Institute, Virginia
    Tech, Blacksburg, USA.}\\
  This has been done, as described in Section~\ref{sec:collab}.
\end{itemize}

\section{Synthetic Sweden resources}
\label{sec:synthswe}

The first steps towards a synthetic population for Sweden were taken
in 2012 when a coarse grained synthetic population for Sweden was
created in a collaboration between the Chalmers LAB group and the
NDSSL Lab at Virginia Tech (where the Los Alamos group had moved).

In this version the smallest geographical unit was ``l\"an'', meaning that
the population was constructed independently for each l\"an. The
synthetic people were then placed within the l\"an, with the result
that demographic differences within the l\"an were not captured.

Furthermore, as no more recent activity survey was found at the time,
the activity patterns were based on a German survey from the early 90s
with the assumption that Swedes and Germans behave more or less the
same way.

During the autumn of 2016 a more detailed synthetic Sweden has been
created. Its main differences from the first version are:
\begin{itemize}
\item \textbf{Higher geographical resolution.} Instead of using l\"an as the
  smallest geographic unit, ``kommun'' is used instead. For the kommun of
  Gothenburg the resolution is even higher as ``prim\"aromr\aa{}de'' is used.
\item Large sample size. A new sample was used for the Iterative
  Proportional Fitting step. The size of this sample was 20 times
  larger than the one used in the first run, ensuring a better
  correlation to the real population.
\item \textbf{Better activity data.} The sample used for Iterative Proportional
  Fitting did not only contain demographic attributes, but also
  information about how people traveled, making it suitable for
  assigning activity patterns to the synthetic individuals.
\item \textbf{Better activity location data.} Data was bought from Sweden
  Statistics (SCB) with detailed information about the larger work
  places in the l\"an of V\"astra G\"otaland. Using this data, the
  provided work places were placed at their exact geographic
  coordinates. The data also states the branches of the different work
  places, providing the possibility to state which work places that doubles as
  locations for shops, schools and other non-working activities.
\end{itemize}

\section{Applications}
\label{sec:apps}

A synthetic population infrastructure is a powerful general framework
for addressing scenario explorations in many diverse application areas
involving \emph{socially coupled systems}:
\begin{description}
\item[Electric vehicles] The adaption of electric cars will introduce
  new demands on infrastructure. By simulating people's movements
  during a day the placement and required number of electric charging
  stations can be analysed. A similar study has been done with a
  synthetic population for the Belgian region of
  Flanders~\cite{flanders}.
\item[Autonomous vehicles] The introduction of autonomous vehicles
  will have a big impact on transportation. Using the
  transportation patterns of a synthetic population, the number of
  autonomous cars for e.g. covering all required trips in the
  Gothenburg area could be investigated. This has previously been done
  for Lisbon~\cite{lisbon}.
\item[Infectious diseases] Using information about how people move and
  interact with each other, the spreading of diseases can be
  modeled. If one person is infected at time A, how will the disease
  have evolved at time B? Since detailed information is available
  about the population one could also simulate e.g. the impact of
  vaccination as is done in~\cite{yi2015fairness}.
\item[Building stock modelling] Much information, including energy
  efficiency, is available for buildings, which can be used for
  constructing models of energy usage in buildings as
  in~\cite{osterbring2016differentiated}. However, very little
  information is available for the actual residents of the
  buildings. The activity patterns of synthetic individuals could be
  used to determine when and why energy is actually used in the
  buildings.
\end{description}

\section{Collaborations}
\label{sec:collab}

The new version of synthetic Sweden has been developed in close
cooperation with Virginia Tech. During September 2016 Oskar Allerbo
visited them in Blacksburg for three weeks, something that sped up the
construction significantly. In February 2017 Madhav Marathe from
Virginia Tech will give a talk about the new population at the
International Conference on Synthetic
Populations\footnote{\url{https://icspconference.wordpress.com/}}
in Lucca, Italy. The conference is organized within the framework
of CoeGSS, the Centre of Excellence for Global Systems
Science\footnote{\url{http://coegss.eu/}}, which is a European 
Commission supported consortium in which both Devdatt Dubashi 
and Oskar Allerbo are participating, that is combining synthetic 
populations with High Performance Computing.\\

At Chalmers, the following collaborations have been initiated:
\begin{description}
\item[Energy] Frances Sprei, Sonia Yeh, Sten Karlsson and Niklas
  Jakobsson from the Department of Energy and Environment have been
  involved during the population construction, to ensure that it can
  be used for electro mobility and autonomous vehicles.
\item[Building Technology] Holger Wallbaum and Magnus \"Osterbring
  from the Division of Building Technology have been represented since
  they are interested in the synthetic population from a city planning
  and household stock and energy perspective.
\item[Life Science and Engineering] We have been in discussions with
  Peter Norberg, Frida Abel, Tomas Bergström, Martin Holmudden and
  Helene Norder at Sahlgrenska for modelling the spreading of infectious
  diseases.
\item[FCC Systems and Data Analysis] The Systems and Data Analysis
  group led by Mats Jirstrand at the Fraunhofer-Chalmers Centre for
  Industrial Mathematics are working on modelling and optimisation of
  energy consumption in buildings. A synthetic population could be of
  potential use here.
\item[Mathematical Biology] We have also been in discussion with
  Marija Cvijovic from the Mathematical Biology group at Mathematical
  Sciences working on systems biology of aging and protein-folding
  disease. Her group see potential benefits of a synthetic population
  containing data such as retirement age and diseases, mainly
  age-related diseases such as neurodegenerative, cardiovascular,
  diabetes, and cancer.
\end{description}

\section{Future steps}
\label{sec:future}

The next steps of synthetic Sweden are further refinement and new
applications.

Further refinement includes the following:
\begin{itemize}
\item \textbf{Acquiring a bigger sample.} SCB can provide very useful samples
  that would be bigger and probably more representative than the one
  used today. It is however associated with two hurdles, the first
  being money and the second privacy. In order to use the sample data
  from SCB the individuals have to be anonymised to such an extent
  that they are not identifiable, but at the same time they cannot be
  too anonymous because then they will not contribute with any
  information to the synthetic population. Here some kind of
  compromise needs to be found.
\item \textbf{Acquiring more detailed activity data.} Today's activity data
  provides information about which locations people are at at a given
  time, but not how they use their time at the location. SCB has a
  time use survey that states how people use their time, something
  that will be very useful e.g. in the building stock application,
  where information about when people do energy consuming activities
  is desired. Here the same to hurdles as above are present.
\item \textbf{Acquiring more work place data.} As mentioned previously work
  place data is bought for the large work places within the l\"an of
  V\"astra G\"otaland. Since this data is available for all of Sweden
  it is just a question of money to include it in the synthetic
  population. Doing this would be very beneficial for the applications
  related to transportation. For instance, the better the locations of
  the work places are known, the higher the certainty for stating
  charging station locations could be.
\item \textbf{Including public transportation information.} Today no public
  transportation is included, but public transportation data for all
  of Sweden is available online for free at
  \url{http://www.trafiklab.se}.
\end{itemize}

Possible future applications are:
\begin{itemize}
\item \textbf{Modelling impact of new roads.} Using information about people's
  traveling patterns, the impact of new solutions in the road network
  can be modelled and used as a support for decision makers.
\item \textbf{Modelling impact of new public transportation routes.} In a
  similar way as above the impact of new solutions within the public
  transportation system could be modelled, e.g. V\"astl\"anken or a
  funicular across the river.
\end{itemize}

\bibliography{synthetic_report}
\bibliographystyle{plain}
\end{document}
