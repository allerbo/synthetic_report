%% Class file for Area of Advance reports
%% Anders Logg 2016-12-20
%% Last changed: 2016-12-21

% Basic setup
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{aoareport}
\LoadClass[a4paper,12pt]{article}

% Packages
\usepackage{times}
\usepackage{a4wide}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{titlesec}
\usepackage[utf8]{inputenc}
\usepackage[font={small,it}]{caption}

% Settings
\usepackage[left=2.0cm, right=2.0cm, top=2.5cm, bottom=2.5cm]{geometry}
\setlength{\parskip}{12pt}
\setlength{\parindent}{0pt}
\setlist{nosep}

% Styling
\definecolor{steelblue}{RGB}{70,130,180}
\definecolor{darksteelblue}{RGB}{49,91,125}
\titleformat*{\section}{\Large\bfseries\sffamily\color{darksteelblue}}
\titleformat*{\subsection}{\large\bfseries\sffamily\color{darksteelblue}}
\titleformat*{\subsubsection}{\normalsize\bfseries\scshape\color{darksteelblue}}
\pagestyle{fancy}

% Commands

\newcommand{\aoatitle}[4]{

  \pagestyle{empty}

  \begin{center}
    \null
    \bigskip

    \Huge \textsc{\textbf{#1}}

    \bigskip

    \Large #2

    \bigskip

    \large #3

    \bigskip

    \Large \today

    \vfill

    \color{darksteelblue} \Large \sf \textbf{#4} \rm

    \bigskip\bigskip

    \includegraphics[width=10cm]{chalmers.pdf}

    \normalsize

  \end{center}

  \newpage
  \null
  \newpage

  \tableofcontents

  \newpage
  \null
  \newpage

  \pagestyle{fancy}

}

\newcommand{\fixme}[1]{
  \noindent
  \begin{minipage}{0.9\textwidth}
    \textbf{FIXME:}
    \it #1
  \end{minipage}
}
